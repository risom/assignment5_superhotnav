﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {


	// drop your spawn points in this array in the inspector
	public GameObject[] spawnPoints;


	public GameObject NPC;

	// Loop through tiles on board/level and displace random game objects
	void Start () {
		
		NPC.tag = ("npc");
		// Grab Spawn points and cout into array
		spawnPoints = GameObject.FindGameObjectsWithTag("floor");

		for(int i = 0; i < 3; i++){

			int rando = Random.Range (0,spawnPoints.Length);
			Vector3 position = new Vector3 (); 
			position = spawnPoints [rando].transform.position;

			// instantiate npc at posistion location
			Instantiate(NPC, position, Quaternion.identity);

		}

	}
		
	// Update is called once per frame
	void Update () {
		
	}
 
}
