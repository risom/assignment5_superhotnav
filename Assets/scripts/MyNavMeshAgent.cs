﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MyNavMeshAgent : MonoBehaviour {


	public NavMeshAgent agent;
	public Transform dest;
	public GameObject[] navPath; // array of places to go
	public int num = 0;
	public float distance;

	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent>();
		navPath = GameObject.FindGameObjectsWithTag ("destination");
	}

	// move to destinations, if reached, move to random destination
	void Update () {

		distance = Vector3.Distance (gameObject.transform.position, navPath [num].transform.position);
		if (distance > 1) {
			dest = navPath [num].transform;
			agent.SetDestination (dest.position);
		}
		else{
			num = Random.Range (0, navPath.Length);
		}
	}
}
