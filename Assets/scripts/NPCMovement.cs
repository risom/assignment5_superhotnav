﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour {


	// Array of waypoints to follow a path
	public GameObject[] path;
	public int num = 0;
	public float speed;
	public float epsilon;

	// find destination objects, add to list
	void Start () {
		path = GameObject.FindGameObjectsWithTag ("destination");
	}
		
	void Update () {
		// distance from destination
		float distance = Vector3.Distance (gameObject.transform.position, path [num].transform.position);

		// if distance is not too small, move towards it
		if (distance > epsilon) {
			MoveToWaypoints ();
		} else {
			if (num == path.Length - 1) {
				num = 0;
			} else { 
				num++;
			}	
		}
	}

	// Have npc look and then move towards object
	public void MoveToWaypoints(){
		gameObject.transform.LookAt (path [num].transform.position);
		gameObject.transform.position += gameObject.transform.forward * Time.deltaTime * 2f;
	}

}
